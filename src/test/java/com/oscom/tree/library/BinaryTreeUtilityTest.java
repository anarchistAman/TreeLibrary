package com.oscom.tree.library;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import com.oscom.tree.binary.impl.BinaryTree;
import com.oscom.tree.binary.impl.BinaryTree.ChildType;
import com.oscom.tree.component.Node;

public class BinaryTreeUtilityTest {
	
public static BinaryTree<Integer> binaryTree;
	
	@BeforeClass
	public static void init() {
		
		binaryTree =  new BinaryTree<>(20);
		binaryTree.addNode(30);
		binaryTree.addNode(40);
		binaryTree.addNode(50);
		binaryTree.addNode(60);
	}
	
	
	@Test
	public void test_find_node_functionality() {
		Node<Integer> node = binaryTree.findNode(50);
		Node<Integer> node1 = binaryTree.findNode(40);
		Node<Integer> node2 = binaryTree.findNode(70);
		
		assertTrue(node.getValue() == 50);
		assertTrue(node1.getValue()==40);
		assertEquals(node2,null);
		
	}
	
	@Test
	public void test_add_node_functionality() {
		Node<Integer> node = binaryTree.findNode(40);
		//System.out.println(node);
		binaryTree.addNode(node,60,ChildType.right);
		
		Node<Integer> newNode = binaryTree.findNode(60);
		//System.out.println(node);
		assertTrue(node.getRightNode().getValue() == 60);
		
		
	}

}
