package com.oscom.tree.library;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.oscom.tree.binary.enums.BinaryTreeView;
import com.oscom.tree.binary.impl.BinaryTree;

public class BinaryTreeViewTest {
	
	public static BinaryTree<Integer> binaryTree;
	
	@BeforeClass
	public static void init() {
		
		binaryTree =  new BinaryTree<>(20);
		binaryTree.addNode(30);
		binaryTree.addNode(40);
		binaryTree.addNode(50);
		binaryTree.addNode(60);
	}
	
	@Test
	public void test_top_view_of_binary_tree() {
		ArrayList<Integer> topView= binaryTree.view();
		List<Integer> topViewExpected = Arrays.asList(new Integer[] {50,30,20,40});
		Boolean isEqual[] = {true};
		
		topView.stream().forEach(e -> {
			if(isEqual[0]) {
				isEqual[0] = (e == topViewExpected.get(topView.indexOf(e)));
			}
		});
		assertEquals(true, isEqual[0]);
		
	}
	
	@Test
	public void test_left_view_of_binary_tree() {
		ArrayList<Integer> leftView= binaryTree.view(BinaryTreeView.left);
		List<Integer> leftViewExpected = Arrays.asList(new Integer[] {20,30,50});
		Boolean isEqual[] = {true};
		
		leftView.stream().forEach(e -> {
			if(isEqual[0]) {
				isEqual[0] = (e == leftViewExpected.get(leftView.indexOf(e)));
			}
		});
		//System.out.println(leftView);
		assertEquals(true, isEqual[0]);
		
	}
	
	@Test
	public void test_right_view_of_binary_tree() {
		ArrayList<Integer> rightView= binaryTree.view(BinaryTreeView.right);
		List<Integer> rightViewExpected = Arrays.asList(new Integer[] {20,40,60});
		Boolean isEqual[] = {true};
		
		rightView.stream().forEach(e -> {
			if(isEqual[0]) {
				isEqual[0] = (e == rightViewExpected.get(rightView.indexOf(e)));
			}
		});
		//System.out.println(rightView);
		assertEquals(true, isEqual[0]);
		
	}
}
