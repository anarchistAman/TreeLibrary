package com.oscom.tree.library;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.oscom.tree.binary.impl.BinaryTree;
import com.oscom.tree.binary.enums.BinaryTreeTraversal;





public class BinaryTreeTraversalTest{
	
	public static BinaryTree<Integer> binaryTree =null;
	
	@BeforeClass
	public static void init() {
		binaryTree =  new BinaryTree<>(20);
		binaryTree.addNode(30);
		binaryTree.addNode(40);
		binaryTree.addNode(50);
		binaryTree.addNode(60);
	}
	
	@Test
	public void test_preorder_traversal() {
		ArrayList<Integer> preOrder = binaryTree.traverse(BinaryTreeTraversal.preorder);
		List<Integer> preOrderExpected = Arrays.asList(new Integer[] {20,30,50,60,40});
		Boolean isEqual[] = {true};
		
		preOrder.stream().forEach(e -> {
			if(isEqual[0]) {
				isEqual[0] = (e == preOrderExpected.get(preOrder.indexOf(e)));
			}
		});
		assertEquals(true, isEqual[0]);
	}
	
	@Test
	public void test_postorder_traversal() {

		ArrayList<Integer> postOrder = binaryTree.traverse(BinaryTreeTraversal.postorder);
		List<Integer> postOrderExpected = Arrays.asList(new Integer[] {50,60,30,40,20});
		Boolean isEqual[] = {true};
		
		postOrder.stream().forEach(e -> {
			if(isEqual[0]) {
				isEqual[0] = (e == postOrderExpected.get(postOrder.indexOf(e)));
			}
		});
		assertEquals(true, isEqual[0]);
		
	}
	
	@Test
	public void test_inorder_traversal() {

		ArrayList<Integer> inOrder = binaryTree.traverse(BinaryTreeTraversal.inorder);
		List<Integer> inOrderExpected = Arrays.asList(new Integer[] {50,30,60,20,40});
		Boolean isEqual[] = {true};
		
		inOrder.stream().forEach(e -> {
			if(isEqual[0]) {
				isEqual[0] = (e == inOrderExpected.get(inOrder.indexOf(e)));
			}
		});
		assertEquals(true, isEqual[0]);
		
	}
	
	@Test
	public void test_level_order_traversal() {
		ArrayList<Integer> levelOrder = binaryTree.traverse();
		List<Integer> levelOrderExpected = Arrays.asList(new Integer[] {20,30,40,50,60});
		Boolean isEqual[] = {true};
		
		levelOrder.stream().forEach(e -> {
			if(isEqual[0]) {
				isEqual[0] = (e == levelOrderExpected.get(levelOrder.indexOf(e)));
			}
		});
		assertEquals(true, isEqual[0]);
		
	}


	

	
}
