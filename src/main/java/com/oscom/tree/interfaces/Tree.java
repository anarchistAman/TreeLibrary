package com.oscom.tree.interfaces;

import java.util.ArrayList;
import com.oscom.tree.component.Node;



public interface Tree<T extends Number> {
	public ArrayList<T> traverse();
	public ArrayList<T> view();
	
}
