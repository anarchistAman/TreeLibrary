package com.oscom.tree.factory;

import com.oscom.tree.binary.impl.BinarySearchTree;
import com.oscom.tree.binary.impl.BinaryTree;
import com.oscom.tree.interfaces.Tree;

public class TreeFactory<T extends Number >{
	


	enum Type{
		Binary,BST;
	}
	
	public static <T extends Number> Tree<T> of(Type type,T data) {
		Tree<T> tree = null;
		if(type.equals(Type.Binary)) {
			tree =  new BinaryTree<T>(data);
		}else if(type.equals(Type.BST)) {
			tree = new BinarySearchTree<T>(data);
		}
		return tree;
	}

}
