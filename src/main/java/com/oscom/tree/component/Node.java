package com.oscom.tree.component;

public class Node<T extends Number> {
	T data;
	Node<T> left,right;
	public Node(T data) {
		this.data = data;
	}
	
	public T getValue() {
		return this.data;
	}
	
	public Node<T> getLeftNode(){
		return this.left;
	}
	
	public Node<T> getRightNode(){
		return this.right;
	}
	
	public Node<T> setLeft(T data) {
		if(this.left == null) {
			Node<T> left = new Node<>(data);
			this.left = left;
		}else {
			this.left.data = data;
		}
		return this.left;
	}
	
	public Node<T> setRight(T data) {
		if(this.right == null) {
			Node<T> right = new Node<>(data);
			this.right = right;
		}else {
			this.right.data = data;
		}
		return this.right;
	}
	
	
	public String toString() {
		StringBuffer nodeDetails = new StringBuffer("[root_details = ").append(this.data);
		if(this.left != null)
			nodeDetails.append(",left_details = ").append(this.left.data);
		else
			nodeDetails.append(",left_details = null");
		if(this.right != null)
			nodeDetails.append(",right_details = ").append(this.right.data);
		else
			nodeDetails.append(",right_details = null");
		nodeDetails.append("]");
		return nodeDetails.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((left == null) ? 0 : left.hashCode());
		result = prime * result + ((right == null) ? 0 : right.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node<?> other = (Node<?>) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (left == null) {
			if (other.left != null)
				return false;
		} else if (!left.equals(other.left))
			return false;
		if (right == null) {
			if (other.right != null)
				return false;
		} else if (!right.equals(other.right))
			return false;
		return true;
	}
	
	
}
