package com.oscom.tree.binary.impl;

import com.oscom.tree.component.Node;

public class BinarySearchTree<T extends Number> extends BinaryTree<T>{
	
	Node<T> root = null;
	public BinarySearchTree(T data) {
		super(data);
		root = new Node<T>(data);
	}
	
	
}
