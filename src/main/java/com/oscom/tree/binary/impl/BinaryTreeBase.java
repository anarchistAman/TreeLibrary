package com.oscom.tree.binary.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.TreeMap;

import com.oscom.tree.binary.enums.BinaryTreeTraversal;
import com.oscom.tree.binary.enums.BinaryTreeView;
import com.oscom.tree.component.Node;
import com.oscom.tree.interfaces.Tree;




public abstract class BinaryTreeBase<T extends Number> implements Tree<T> {

	public abstract Node<T> findNode(T data);
	//public abstract Node<T> findCommonAncesstor(Node<T> node1,Node<T> node2);

	protected Node<T> root = null;
	BinaryTreeBase(T data){
		root = new Node<>(data);
	}

	public ArrayList<T> traverse(){
		return levelorder();	
	}

	/**
	 * This function is used to traverse a binary tree in a specific manner.
	 * Binary Tree can be traversed in following ways.
	 * 1.Inorder Traversal
	 * 2.PreOrder Traversal
	 * 3.PostOrder Traversal
	 * 4.LevelOrder Traversal
	 * 
	 *  @param
	 *   traverse : {@link BinaryTreeTraversal} 
	 *  
	 *  @return
	 *  ArrayList<T> : List of visited nodes during the traversal.
	 * **/
	public final ArrayList<T> traverse(BinaryTreeTraversal traverse){
		ArrayList<T> traversedNodeData = new ArrayList<>();
		if(traverse == BinaryTreeTraversal.inorder) {
			inOrderUtil(this.root, traversedNodeData);
		}else if(traverse == BinaryTreeTraversal.postorder) {
			postOrderUtil(this.root, traversedNodeData);
		}else if(traverse == BinaryTreeTraversal.preorder) {
			preOrderUtil(this.root, traversedNodeData);
		}
		return traversedNodeData;
	}
	/**
	 * This function retruns the default view for the binary tree.
	 * The default view for the binary tree is left view
	 * 
	 * **/
	public final ArrayList<T> view(){
		TreeMap<Integer,T> map = new TreeMap<>();
		topViewUtil(this.root,map);
		ArrayList<T> topView = new ArrayList<>();
		map.entrySet().stream().forEach(e->{
			topView.add(e.getValue());
		});
		return topView;	
	}

	private void topViewUtil(Node<T> root,TreeMap<Integer,T> map){
		class QueueObj{
			Node<T> node;
			int hd;
			QueueObj(Node<T> node,int hd) {
				this.node=node;
				this.hd=hd;
			}

		}
		Queue<QueueObj> queue = new LinkedList<>();
		QueueObj obj= new QueueObj(root,0);
		queue.add(obj);

		while(!queue.isEmpty()) {
			QueueObj object = queue.poll();
			if (!map.containsKey(object.hd)) { 
				map.put(object.hd, object.node.getValue()); 
			}

			if(object.node.getLeftNode()!=null) {
				queue.add(new QueueObj(object.node.getLeftNode(),object.hd-1));
			}
			if(object.node.getRightNode()!=null) {
				queue.add(new QueueObj(object.node.getRightNode(),object.hd+1));
			}
		}
	}

	/**
	 * This is an overloaded function which will return ArrayList of node data based on the selected view.
	 * List of view are {@link BinaryTreeView}
	 * **/
	public ArrayList<T> view(BinaryTreeView view){
		
		ArrayList<T> viewArray = new ArrayList<>();
		switch(view) {
			case top: 	viewArray =  view();
						break;
			case left:	viewArray = leftViewUtil();
						break;
			
			case right: viewArray = rightViewUtil();
						break;
			
			default:  viewArray = null;
		}
		return viewArray;

	}
	
	/**
	 * This is a utility function which will return the list of node->data of left view of the tree.
	 * 
	 * @return
	 * ArrayList<T>
	 * **/
	private ArrayList<T> leftViewUtil(){
		ArrayList<T> viewDataList = new ArrayList<>();
		Queue<Node<T>> queue = new LinkedList<>();
		queue.add(this.root);
		while(!queue.isEmpty()) {
			int size =0;
			size = queue.size();
			boolean isLeftRead = false;
			while(size>0) {
				Node<T> n = queue.poll();
				if(n.getLeftNode() != null)
					queue.add(n.getLeftNode());
				if(n.getRightNode() != null)
					queue.add(n.getRightNode());
				if(!isLeftRead) {
					viewDataList.add(n.getValue());
					isLeftRead = true;
				}
			size--;
			}
		}
		return viewDataList;
	}
	
	/**
	 * This is a utility function which will return the list of node->data of right view of the tree.
	 * 
	 * @return
	 * ArrayList<T>
	 * **/
	private ArrayList<T> rightViewUtil(){
		ArrayList<T> viewDataList = new ArrayList<>();
		Queue<Node<T>> queue = new LinkedList<>();
		queue.add(this.root);
		while(!queue.isEmpty()) {
			int size =0;
			size = queue.size();
			
			while(size>0) {
				Node<T> n = queue.poll();
				if(n.getLeftNode() != null)
					queue.add(n.getLeftNode());
				if(n.getRightNode() != null)
					queue.add(n.getRightNode());
				if(size == 1) {
					viewDataList.add(n.getValue());
				}
			size--;
			}
		}
		return viewDataList;
	}

	/**
	 * This method returns the level order traversal of the tree
	 * 
	 * @return
	 * ArrayList<T> : - Level-Order Traversal.
	 * **/
	private ArrayList<T> levelorder(){
		Queue<Node<T>> queue = new LinkedList<>();
		ArrayList<T> level_order_traversal = new ArrayList<>();
		queue.add(this.root);
		while(!queue.isEmpty()) {
			Node<T> node = queue.poll();
			level_order_traversal.add(node.getValue());
			if(node.getLeftNode() != null) {
				queue.add(node.getLeftNode());
			}
			if(node.getRightNode() != null)
				queue.add(node.getRightNode());

		}
		return level_order_traversal;
	}



	/**
	 * A utility which will traverse the tree in postOrder fashion and add the data of traversed nodes to the ArrayList.
	 * <br>
	 * Traversal :- <b>left-->right-->root</b>
	 * @param Node<T> root :-Root node of the tree which needs to be traversed.
	 * @param ArrayList<T> :- Data of the nodes which has been added to list while traversing the tree in postorder fashion.  
	 * 
	 * @return
	 * NULL
	 * **/
	private void postOrderUtil(Node<T> node,ArrayList<T> list) {
		if(node == null) {
			return;
		}

		postOrderUtil(node.getLeftNode(),list);
		postOrderUtil(node.getRightNode(),list);	
		list.add(node.getValue());
	}

	/**
	 * A utility which will traverse the tree in preOrder fashion and add the data of traversed nodes to the ArrayList.
	 * <br>
	 * Traversal algo:- <b>root-->left-->right</b>
	 * @param Node<T> root :-Root node of the tree which needs to be traversed.
	 * @param ArrayList<T> :- Data of the nodes which has been added to list while traversing the tree in preorder fashion.  
	 * 
	 * @return
	 * NULL
	 * **/
	private void preOrderUtil(Node<T> node,ArrayList<T> list) {
		if(node == null) {
			return;
		}
		list.add(node.getValue());
		preOrderUtil(node.getLeftNode(),list);
		preOrderUtil(node.getRightNode(),list);	
	}


	/**
	 * A utility which will traverse the tree in inorder fashion and add the data of traversed nodes to the ArrayList.
	 * <br>
	 * Traversal algo:- <b>left-->root-->right</b>
	 * @param Node<T> root :-Root node of the tree which needs to be traversed.
	 * @param ArrayList<T> :- Data of the nodes which has been added to list while traversing the tree in inorder fashion.  
	 * 
	 * @return
	 * NULL
	 * **/
	private void inOrderUtil(Node<T> node,ArrayList<T> list) {
		if(node == null) {
			return;
		}
		inOrderUtil(node.getLeftNode(),list);
		list.add(node.getValue());
		inOrderUtil(node.getRightNode(),list);	
	}



}
