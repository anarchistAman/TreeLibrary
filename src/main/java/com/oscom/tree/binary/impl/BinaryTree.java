package com.oscom.tree.binary.impl;

import java.util.LinkedList;
import java.util.Queue;

import com.oscom.tree.component.Node;


public class BinaryTree<T extends Number> extends BinaryTreeBase<T> {

	public enum ChildType{
		left,
		right;
	}
	
	public BinaryTree(T data) {
		super(data);

	}

	
	/**
	 * This method is used to add a node as a left or right child, it will insert the node at
	 * first found empty position from the root.
	 * 
	 * @return
	 * Node<T> : Added node in the binary tree.
	 * **/
	public Node<T> addNode(T data) {
		Queue<Node<T>> queue = new LinkedList<>();
		queue.add(this.root);
		boolean isInserted = false;
		while(!queue.isEmpty() && !isInserted) {
			Node<T> node = queue.poll();
			if(node.getLeftNode() == null) {
				node.setLeft(data);
				isInserted = true;
				continue;
			}else {
				queue.add(node.getLeftNode());
			}
			if(node.getRightNode() == null) {
				node.setRight(data);
				isInserted = true;
				continue;
			}else {
				queue.add(node.getRightNode());
			}
		}
		return root;
	}
	
	public Node<T> addNode(Node<T> parent,T data,ChildType child){
		if(parent == null)
			return null;
		if(child == ChildType.left) {
			return parent.setLeft(data);
		}else {
			return parent.setRight(data);
		}
	}



	@Override
	public Node<T> findNode(T data) {return findNode(data,this.root);}
	
	private Node<T> findNode(T data,Node<T> root){
		if(root == null) {
			return null;
		}
		Node<T> node = null;
		if(root.getValue() == data) {
			node = root;
		}else {
			node = findNode(data,root.getLeftNode());
			if(node == null)
				node = findNode(data,root.getRightNode());
		}
		return node;
	}







}
