package com.oscom.tree.binary.enums;

public enum BinaryTreeTraversal {
	inorder,
	preorder,
	postorder;
}

