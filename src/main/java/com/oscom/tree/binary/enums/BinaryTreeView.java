package com.oscom.tree.binary.enums;

public enum BinaryTreeView {
	
	left,
	right,
	top;
}
