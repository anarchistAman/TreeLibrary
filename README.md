# Tree Library
<pre>
It's a library project.This library provides predefined implementation for Tree data-structure.
The library contains predefined set of functions for following tasks:
1.Can create a tree of following type.<br>
    i.Binary Tree.
    ii. Binary Search Tree.
    iii.AVL Tree.
    
2.Using this libary one can create a tree of Integer,Long,Double,Float,LongInt data types .  
3.Add data to the created tree.The algorithm being used to insert the data is similar to the standard ones.
4.Find the common parent for two different nodes.
5.Do all types traversal supported by above mention tree types.
6.It has pedefined function to get  top,left and right view of the tree.
7.It has a predefined function for generating the mirror image a existing tree.
8.It has feature for checking where a tree of type BinaryTree or Binary Search Tree is height balanced or not.
</pre>
## Getting Started

Clone the project and import it in any of the IDE of your choice.

### Prerequisites
```
1.maven
2.Java 8
```
## Running the tests

Go to project directory and run the following command:
```
$ maven install 
```
## Built With
```
* [Maven](https://maven.apache.org/) - Dependency Management
```

## Authors

* **Aman Choudhary** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

